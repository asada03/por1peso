var formSubmitted = 5;

var usuario = "andresjunk";
var cantidadPujar = 0.05;
var maxPujas = 50;
var bidsPerPuja = 3;
var idBoton = "s399074259";
var idTimer = "t399074259";
var idPrice = "px399074259";
var idWinner = "w399074259";
var timerTrigger = "00:00:01";

var description = "";

var noPujas = 0;
var numPujas = 0;
var currPrice = 0;
var lastPrice = 0;

var lastTimer = -1;
var thisTimer = -1;
var realTimer = 999999;
var timerTick = 0;
var yaPuje = false;

var updateTimerId = 0;

var pujare = false;

var bidders = [];



function getPrice(price)
{
	return parseFloat(price.slice(2).replace(",",""));
}

function receiveMsg(aMessageEvent) 
{
	
	console.log("got message with " + aMessageEvent.message)
   	if (aMessageEvent.name === "productId") 
	{
		idBoton = "s" + aMessageEvent.message;
		idTimer = "t" + aMessageEvent.message;
		idPrice = "p" + aMessageEvent.message;
		idWinner = "w" + aMessageEvent.message;
		
    	document.getElementById("asgprice").innerHTML = "DETENIDO " + document.getElementById(idPrice).innerHTML + " " + document.getElementById(idTimer).innerHTML  + " " + document.getElementById(idWinner).innerHTML;	
        console.log("Received Product Id " + " " + idBoton  + " " + idTimer  + " " + idPrice  + " " + idWinner);
    }

    else if (aMessageEvent.name === "usuario")
    {
        usuario = aMessageEvent.message.trim();
        console.log("setting usuario to:" + usuario)
    }
    
    else if (aMessageEvent.name === "increment")
    {
        cantidadPujar = parseFloat(aMessageEvent.message);
        console.log("setting increment to:" + cantidadPujar)
    } 
    
    else if (aMessageEvent.name === "maxPujas")
	{
		numPujas = 0;
		maxPujas = parseInt(aMessageEvent.message);
		console.log("parsing message for pujas:" + aMessageEvent.message + " to:" + maxPujas);
		console.log("setting Max Pujas to:" + maxPujas);
		document.getElementById("asgpujas").innerHTML = numPujas;
		document.getElementById("asgmaxPujas").innerHTML = maxPujas;
		
        startApp();
		
	}

	else if (aMessageEvent.name === "startApp")
    {
		startApp();
        var derscTable = document.getElementsByTagName("title")[0].innerHTML;
        description = derscTable.slice(derscTable.search(" - ")+3);
        safari.self.tab.dispatchMessage("willBid",description);
    }
    
	
	else if (aMessageEvent.name === "stopApp")
		stopApp();
}

function readTable()
{
    timerTick++;
    var clock = document.getElementById(idTimer).innerHTML;
	thisTimer = parseInt("0" + clock.replace(/:/g,""),10);
    
    if (thisTimer < realTimer)
    {
        realTimer = thisTimer;
        timerTick = 1;
    }
    else if (thisTimer > lastTimer)
    {
        realTimer = thisTimer;
        timerTick = 1;
        yaPuje = false;
    }
    else if (timerTick == 4)
    {
        realTimer--;
        timerTick = 1;
    }
	
	if ( ((realTimer == 1 && timerTick == 2) || (realTimer == 0 && timerTick == 1)) && !yaPuje)
	{
        yaPuje = true;
		if (pujare && document.getElementById(idWinner).innerHTML.trim() != usuario )
        {
            // document.forms["asgform"].submit();
            document.getElementById(idBoton).click();
            console.log("will click button and bid, timer = " + thisTimer + ":" + realTimer + ", " + document.getElementById(idWinner).innerHTML.trim() + "==" + usuario );
            if (++numPujas == maxPujas || parseInt(document.getElementById("stateBarBidsCount").innerHTML,10) < bidsPerPuja)
            {
                console.log("Ya no podré pujar, numPujas:" + numPUjas + " Bids restantes:" + parseInt(document.getElementById("stateBarBidsCount").innerHTML,10));
                stopApp();
            }
            
            document.getElementById("asgprice").innerHTML = "PUJÉ";
            document.getElementById("asgpujas").innerHTML = numPujas;
        }
        else
        {
            console.log("No puje at " + thisTimer + ":" + realTimer + ", " + pujare + ", " + document.getElementById(idWinner).innerHTML.trim() + "==" + usuario );
            document.getElementById("asgnoPujas").innerHTML = ++noPujas;
        }
	}
	
    if (lastTimer != thisTimer)
    {
        lastTimer = thisTimer;

        // read the table
        var theTable = document.getElementById("biddersTable").getElementsByTagName("table");
        var tableElements = theTable[0].getElementsByTagName("tr");
        var x = 0;
        var y = 0;
        var z = 0;
        var w = 0;
        var autoCtr = 0;
        var trColor = "";
        var trName = "";
        var isAutopuja = false;
        var tableSize = 0;
        
        
        
        if (tableElements.length == 0)
        {
            console.log("Empty table");
            programBid();
        }
        else
        {
            // count the new elements in the table
            
            for (x=0;  x < tableElements.length && lastPrice != getPrice(tableElements[x].getElementsByTagName("td")[1].innerHTML); x++); // intentional semicollen
            var sumTable = document.getElementById("asgBidders");
            var rowElements = sumTable.getElementsByTagName("tr");
            console.log("Found " + x + " new Elements at " + thisTimer + ":" + realTimer);
            y = x - 1;
            while ( y >= 0)
            {
                trColor = tableElements[y].className;
                autoCtr = 0;
                do
                {
                    trName = tableElements[y].getElementsByTagName("td")[0].innerHTML; 
                    console.log("Found" + trName);
                    
                    // see if it is Autopuja
                    isAutopuja = (trName.search("Autopuja") > -1);
                    if (isAutopuja)
                    {
                        autoCtr++;
                        console.log("Is Autopuja");
                    }
                    
                    // count this user
                    tableSize = bidders.length;
                    console.log ("Will search in a table of size " + tableSize );
                    for (z=0; z < tableSize; z++)
                    {
                        console.log ("Is " + trName + " == "+ bidders[z][0]);
                        if (trName.search(bidders[z][0]) > -1)
                        {
                            bidders[z][1]++;
                            if (isAutopuja)
                                bidders[z][2]++;
                            var cellElements = rowElements[z].getElementsByTagName("td");
                            cellElements[1].innerHTML = bidders[z][1] + "";
                            cellElements[2].innerHTML = bidders[z][2] + "";
                            cellElements[3].innerHTML = (bidders[z][1]*bidsPerPuja*.7)+""
                            break;
                        }
                    }
                    if (z == tableSize)
                    {
                        var justName = trName.split("(");
                        console.log ("justName " + justName + "..." + justName[0].trim());
                        var bidCount = [justName[0].trim(),1,0]; // name, bids, num
                        if (isAutopuja)                       
                            bidCount[2]++;
                       
                        console.log("Bid Count = " + bidCount);
                        bidders[z] = bidCount;
                        console.log("Found New One, Table size is now " + bidders.length);
                        var row = document.createElement("tr");
                        var cell1 = document.createElement("td");
                        var cell2 = document.createElement("td");
                        var cell3 = document.createElement("td");
                        var cell4 = document.createElement("td");
                        
                        cell1.innerHTML = bidCount[0];
                        row.appendChild(cell1);
                        cell2.innerHTML = bidCount[1]+"";
                        row.appendChild(cell2);
                        cell3.innerHTML = bidCount[2]+"";
                        row.appendChild(cell3);
                        cell4.innerHTML = (bidCount[1]*bidsPerPuja*.7)+"";
                        row.appendChild(cell4);
                        sumTable.appendChild(row);
                    }
                    y--;
                    
                }while (y >= 0 && trColor == tableElements[y].className);
                
                console.log("Auto Counter = " + autoCtr);
                if (autoCtr == 1)
                {
                    programBid();
                }
                else if (autoCtr > 1)
                {
                    stopBid();
                }
                // otherwise don´t change
                
                for (z=0; z < bidders.length; z++)
                    console.log("Table " + bidders[z]);
                
            }	
            
            lastPrice = getPrice(tableElements[0].getElementsByTagName("td")[1].innerHTML);
            
        }
    }
        
    if (thisTimer == 0 && clock.trim() == "Terminada")
    {
        console.log("Subasta Terminada");
        stopAp();
    }
        
}

function startApp()
{       
	if (updateTimerId)
		console.log("Already initialized");
	else
    {
		updateTimerId = setInterval(readTable, 333);
        pujare = false;
        document.getElementById("asgprice").innerHTML = "PUJARÉ";
        console.log("Starting App with " + description);
    }  
}

function stopApp()
{
    console.log("Will Stop App");
	if (updateTimerId)
		updateTimerId = clearInterval(updateTimerId);
	else
		console.log("Already stopped");
	
	document.getElementById("asgprice").innerHTML = "DETENIDO";
	
	// get ready in case it starts again
    yaPuje = false;
    lastTimer = -1;
    thisTimer = -1;
    realTimer = 999999;
    timerTick = 0;
    lastPrice = 0;
}

function programBid()
{
	if (!pujare)
	{
		pujare = true;
		document.getElementById("asgprice").innerHTML = "PUJARÉ";
		console.log("asg PUJARÉ");
	}
}	

function stopBid()
{
	if (pujare)
	{
		pujare = false;
		document.getElementById("asgprice").innerHTML = "NO PUJARÉ";
		console.log("asg NO PUJARÉ");
	}
}	

function modifyTab() {
	// get the reference for the body
	var tblBody = document.getElementById("stateBarBidsCount").parentNode.parentNode.parentNode.parentNode.parentNode.parentNode;
	var row = document.createElement("tr");
	row.style="height:100"
	var newElement = document.createElement("p");
	newElement.innerHTML = "<span id='asgprice'>DETENIDO</span> \
	pujas <span id='asgpujas'>0</span> de <span id='asgmaxPujas'>50</span>, <span id='asgnoPujas'>0</span> ignoradas </div> \
    <embed name='asgbeep' src='http://www.disenatuimagen.net/beepbeep.wav' autostart='false' loop='false' height='6' width='6'>";
	
	var cell = document.createElement("td");
	cell.style="height:100"
	row.appendChild(cell);
	cell = document.createElement("td");
	cell.style="height:100"
	cell.colSpan = "6";
	cell.className = "state_bar_middle";
	cell.cellSpacing = "0";
	cell.cellPadding = "0";
	cell.appendChild(newElement);
	row.appendChild(cell);
	// add the row to the end of the table body
	tblBody.appendChild(row);

    tblBody = document.getElementById("biddersTable").parentNode;

    var divElement = document.createElement("div");
    divElement.className = "bg";
    divElement.innerHTML = "<table id='asgBidders' class='bidders_half' cellpadding='0' cellspacing='0'><tbody></tbody></table>";
    //tblBody.appendChild(divElement);
    document.body.insertBefore(divElement, document.body.firstChild);
    
    

    /*
    var table = document.createElement("table");
    table.className = "bidders_half";
    table.cellPadding = "0";
    table.cellSpacing = "0";
    table.id = "asgBidders";
    div.appendChild(table);
    tblBody.appendChild(div);
    
    row = document.createElement("tr");
    cell = document.createElement("td");
    
    cell.innerHTML = "Andres";
    row.appendChild(cell);
    cell.innerHTML = 20+"";
    row.appendChild(cell);
    cell.innerHTML = 20+"";
    row.appendChild(cell);
    cell.innerHTML = (20*bidsPerPuja*.7)+"";
    row.appendChild(cell);
    table.appendChild(row);
     */
    
    console.log (tblBody.innerHTML);
}



//document.body.insertBefore(newElement, document.body.firstChild);
modifyTab();

//<div class="bg" id="biddersTable"><table class="bidders_half" cellpadding="0" cellspacing="0"><tbody><tr class="bidRow9"><td>dryblas (Autopuja)</td><td class="right">$ 617.45 <span>MXN</span></td><td class="bidders_date">2011-02-25 01:42:34</td></tr><tr class="bidRow9"><td>dalkun (Autopuja)</td><td class="right">$ 617.40 <span>MXN</span></td><td class="bidders_date">2011-02-25 01:42:34</td></tr><tr class="bidRow7"><td>dryblas (Autopuja)</td><td class="right">$ 617.35 <span>MXN</span></td><td class="bidders_date">2011-02-25 01:42:24</td></tr><tr class="bidRow7"><td>dalkun (Autopuja)</td><td class="right">$ 617.30 <span>MXN</span></td><td class="bidders_date">2011-02-25 01:42:24</td></tr><tr class="bidRow5"><td>dryblas (Autopuja)</td><td class="right">$ 617.25 <span>MXN</span></td><td class="bidders_date">2011-02-25 01:42:14</td></tr><tr class="bidRow5"><td>dalkun (Autopuja)</td><td class="right">$ 617.20 <span>MXN</span></td><td class="bidders_date">2011-02-25 01:42:14</td></tr><tr class="bidRow3"><td>dryblas (Autopuja)</td><td class="right">$ 617.15 <span>MXN</span></td><td class="bidders_date">2011-02-25 01:42:04</td></tr><tr class="bidRow3"><td>dalkun (Autopuja)</td><td class="right">$ 617.10 <span>MXN</span></td><td class="bidders_date">2011-02-25 01:42:04</td></tr><tr class="bidRow1"><td>dryblas (Autopuja)</td><td class="right">$ 617.05 <span>MXN</span></td><td class="bidders_date">2011-02-25 01:41:54</td></tr><tr class="bidRow1"><td>dalkun (Autopuja)</td><td class="right">$ 617.00 <span>MXN</span></td><td class="bidders_date">2011-02-25 01:41:54</td></tr></tbody></table></div>

// register for message events
safari.self.addEventListener("message", receiveMsg, false);





